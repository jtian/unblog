# unblog

jtian's (un)blog


# why GitLab (or why not GitHub)

1. As is [pointed out](https://docs.gitlab.com/ee/user/markdown.html#math), GitLab supports $`\TeX`$ rendering. GitHub, are you afraid of burning my browser or simply lazy to implement?
2. Smaller & proper default font size, with less padding that does not make sense.
